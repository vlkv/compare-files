const {
  readFile,
} = require('fs');
const {
  promisify,
} = require('util');

const promisifiedReadFile = promisify(readFile);

/**
 * This module compare files and display difference to console
 *
 * @author                        Sergii Volkov <vlkv.dev@icloud.com>
 * @param {string[]} fileList     List of files, which need to compare
 * @returns {undefined}           Result will be displayed in console
 */
module.exports = (fileList) => {
  if (fileList.length < 2) {
    return console.log('Two files must be provided as arguments');
  } else if (fileList.length > 2) {
    fileList = fileList.splice(0, 2);
    console.log(`Two first files will be compared: ${fileList}`);
  }

  const fileReadPromises = fileList.map(pathToFile => promisifiedReadFile(pathToFile, {encoding: 'utf-8'}));

  Promise.all(fileReadPromises)
    .then(fileContents => {
      const fileInformation = fileContents.map(content => content.split('\n'));

      displayDiffBetweenLines(fileInformation);

    })
    .catch(err => {
      const fileErrors = ['ENOENT', 'EACCES'];

      if (err.code && fileErrors.includes(err.code)) {
        console.log(`Check your files: ${err.code} ${err.path}`);
      } else {
        console.log('An error has been occurred', err);
      }
    });
};

/**
 * Compare lines from two arrays and display difference between them.
 *
 * Go through all lines from File#1.
 * If current line is identical to appropriate line from File#2 or File#1 contains appropriate line from File#2 - don't
 * display any symbols and display current line from File#1
 * Otherwise, if File#1 doesn't contain current line from File#2 - display `*` symbol and two lines with pipe symbol
 * For all lines from File#2 which don't exist in File1 - display `+` symbol and line from File#2.
 * For all lines from File#1 which don't exist in File2 - display `-` symbol and line from File#1.
 *
 * @param {string[]} firstFileLines     Lines from first file
 * @param {string[]} secondFileLines    Lines from second file
 *
 * @returns {undefined}
 */
function displayDiffBetweenLines([ firstFileLines, secondFileLines ]) {
  firstFileLines.forEach((firstFileLine, currentIndex) => {
    const lineNumber = currentIndex + 1;
    let changes = '';

    const secondFileLine = secondFileLines[currentIndex];

    // if first file is greater than second, display this delta
    if (secondFileLine === undefined) {
      console.log(`${lineNumber} - ${firstFileLine}`);
      return;
    }

    if (firstFileLine === secondFileLine) {
      changes = `  ${firstFileLine}`;
    } else {
      const firstFileLineInSecondFile = secondFileLines.includes(firstFileLine);
      const secondFileLineInFirstFile = firstFileLines.includes(secondFileLine);

      if (firstFileLineInSecondFile) {
        changes = `  ${firstFileLine}`
      } else {
        if (secondFileLineInFirstFile) {
          changes = `- ${firstFileLine}`;
        } else {
          changes = `* ${firstFileLine} | ${secondFileLine}`;
        }
      }
    }

    console.log(`${lineNumber} ${changes}`);
  });

  const firstFileLinesLength = firstFileLines.length;
  const differenceBetweenFiles = secondFileLines.length - firstFileLinesLength;

  // if second file is greater than first, display this delta
  if (differenceBetweenFiles > 0) {
    secondFileLines.slice(firstFileLinesLength - 1).forEach((secondFileLine, index) => {
      const secondFileLineIndex = index + firstFileLinesLength + 1;

      console.log(`${secondFileLineIndex} + ${secondFileLine}`);
    });
  }

}
