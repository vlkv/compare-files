# compare-files module

## Usage
```node index.js sample/sample1.log sample/sample2.md```

**You need to use Node.js greater than v8.0.0**

A branch `feature/async-await-me` has an async/await syntax instead of then/catch using.
