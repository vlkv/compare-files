const compareFiles = require('./lib/compare-files');

// get files from arguments list
const fileList = process.argv.slice(2);
compareFiles(fileList);
